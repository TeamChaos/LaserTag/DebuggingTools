#include <Wire.h>



/*
 * Copyright (c) 2015, Majenko Technologies
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * * Neither the name of Majenko Technologies nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Create a WiFi access point and provide a web server on it. */


//Basic
#include <WiFiClient.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include "wifi.h"

//SPECIFIC
//#include "brzo_i2c.h"
#include <Wire.h>
#include "detector_control/protocol.h"

#include <RCSwitch.h>

// radio headers
#define SIG_SETUP_FINISHED 0
#define SIG_TIMER_STARTED 1
#define SIG_HIT 2
#define SIG_DEATH 3
#define SIG_GAME_OVER 4

//Wifi Configuration
const char* ssid = WIFI;
const char* password = WIFI_PASS;

const char* ssid_ap = "LaserTag_Debugger1";
const char* password_ap = "minecraft95";


//Telnet Configuration
#define INPUT_SIZE 50
#define MAX_TELNET_CLIENTS 1
WiFiServer TelnetServer(23);
WiFiClient TelnetClient[MAX_TELNET_CLIENTS];
uint8_t i;
bool ConnectionEstablished; // Flag for successfully handled connection

String telnetSendQueue="";






//Predefine basic methods
void TelnetMsg(String text);
void blinkLED();
void Telnet();

void setupSpecific();
void loopSpecific();
void onTelnetMessage(char* buffer);
void decodeRadioSignal(uint32_t receivedSignal);



//------------------------ BASIC MAIN -------------------------------------



void setup()
{
  Serial.begin(115200);
  Serial.println("Over The Air and Telnet Example");

  Serial.printf("Sketch size: %u\n", ESP.getSketchSize());
  Serial.printf("Free size: %u\n", ESP.getFreeSketchSpace());

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  // ... Give ESP 10 seconds to connect to station.
  unsigned long startTime = millis();
  Serial.print("Waiting for wireless connection ");
  while (WiFi.status() != WL_CONNECTED && millis() - startTime < 10000)
  {
    delay(200);
    Serial.print(".");
  }
  Serial.println();

  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.println("Connection Failed! Rebooting...");
    Serial.print("Configuring access point...");
    /* You can remove the password parameter if you want the AP to be open. */
    WiFi.softAP(ssid_ap, password_ap);

   IPAddress myIP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(myIP);
    //delay(3000);
    //ESP.restart();
  }

  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Starting Telnet server");
  TelnetServer.begin();
  TelnetServer.setNoDelay(true);

  pinMode(BUILTIN_LED, OUTPUT);  // initialize onboard LED as output

  // OTA

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  // ArduinoOTA.setHostname("myesp8266");

  // No authentication by default
  //ArduinoOTA.setPassword((const char *)"test");

  ArduinoOTA.onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH) {
        type = "sketch";
      } else { // U_SPIFFS
        type = "filesystem";
      }

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    });
    ArduinoOTA.onEnd([]() {
      Serial.println("\nEnd");
    });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    });
    ArduinoOTA.onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) {
        Serial.println("Auth Failed");
      } else if (error == OTA_BEGIN_ERROR) {
        Serial.println("Begin Failed");
      } else if (error == OTA_CONNECT_ERROR) {
        Serial.println("Connect Failed");
      } else if (error == OTA_RECEIVE_ERROR) {
        Serial.println("Receive Failed");
      } else if (error == OTA_END_ERROR) {
        Serial.println("End Failed");
      }
    });
  ArduinoOTA.begin();

  setupSpecific();
}

void loop() {
  ArduinoOTA.handle(); // Wait for OTA connection
  Telnet();  // Handle telnet connections
  if(telnetSendQueue.length() >0){
    TelnetMsg(telnetSendQueue);
    telnetSendQueue="";
  }
  loopSpecific();
}

void TelnetMsg(String text)
{
  for(i = 0; i < MAX_TELNET_CLIENTS; i++)
  {
    if (TelnetClient[i] && TelnetClient[i].connected())
    {
      TelnetClient[i].print(text);
    }
  }
  delay(10);  // to avoid strange characters left in buffer
}

void Telnet()
{
  // Cleanup disconnected session
  for(i = 0; i < MAX_TELNET_CLIENTS; i++)
  {
    if (TelnetClient[i] && !TelnetClient[i].connected())
    {
      Serial.print("Client disconnected ... terminate session "); Serial.println(i+1);
      TelnetClient[i].stop();
    }
  }

  // Check new client connections
  if (TelnetServer.hasClient())
  {
    ConnectionEstablished = false; // Set to false

    for(i = 0; i < MAX_TELNET_CLIENTS; i++)
    {
      // Serial.print("Checking telnet session "); Serial.println(i+1);

      // find free socket
      if (!TelnetClient[i])
      {
        TelnetClient[i] = TelnetServer.available();

        Serial.print("New Telnet client connected to session "); Serial.println(i+1);

        TelnetClient[i].flush();  // clear input buffer, else you get strange characters
        TelnetClient[i].println("Welcome!");

        TelnetClient[i].print("Millis since start: ");
        TelnetClient[i].println(millis());

        TelnetClient[i].print("Free Heap RAM: ");
        TelnetClient[i].println(ESP.getFreeHeap());

        TelnetClient[i].println("----------------------------------------------------------------");

        ConnectionEstablished = true;

        break;
      }
      else
      {
        // Serial.println("Session is in use");
      }
    }

    if (ConnectionEstablished == false)
    {
      Serial.println("No free sessions ... drop connection");
      TelnetServer.available().stop();
      // TelnetMsg("An other user cannot connect ... MAX_TELNET_CLIENTS limit is reached!");
    }
  }

  for(i = 0; i < MAX_TELNET_CLIENTS; i++)
  {
    if (TelnetClient[i] && TelnetClient[i].connected())
    {
      if(TelnetClient[i].available())
      {
        //get data from the telnet client
        while(TelnetClient[i].available())
        {
          char buffer[INPUT_SIZE + 1];
          byte size = TelnetClient[i].readBytesUntil('\n', buffer, INPUT_SIZE); //Probably should not use String because overhead
          buffer[size]=0;//Add 0 for c string end
          onTelnetMessage(buffer);
        }
      }
    }
  }
}

int ledState = LOW;

void blinkLED()
{
    // toggle the LED
    ledState = !ledState;
    digitalWrite(BUILTIN_LED, ledState);
    delay(20);

}

//#######################################################################################################################
//------------- END MAIN ------------------------------------------------------------------------------------------------
//#######################################################################################################################



//------------- START SPECIFIC ------------------------


#define BYTE_TO_BINARY_PATTERN "%c%c%c%c %c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')

#define SDA_PIN 4
#define SCL_PIN 5
#define TRANSMIT_PIN 12
#define SCL_FREQ 100
#define RADIO_RECEIVE_PIN 13


//radio
RCSwitch rcs = RCSwitch();


void setupSpecific(){
  pinMode(TRANSMIT_PIN,INPUT);
  //brzo_i2c_setup(SDA_PIN, SCL_PIN, 2000);
  Wire.setClock(100000);
  Wire.begin();

  rcs.enableReceive(RADIO_RECEIVE_PIN);
}

void decodeRadioSignal(uint32_t receivedSignal) {

  // masks to retrieve signal components
  uint32_t typeMask = 0x07;            // 0000 0000 0000 0000 0000 0000 0000 0111;
  //type hit
  uint32_t victimIDMask = 0x078;       // 0000 0000 0000 0000 0000 0000 0111 1000;
  uint32_t killsMask = 0x07F80;        // 0000 0000 0000 0000 0111 1111 1000 0000;
  uint32_t offenderIDMask = 0x078000;  // 0000 0000 0000 0111 1000 0000 0000 0000;
  uint32_t assistsMask = 0x3F80000;    // 0000 0011 1111 1000 0000 0000 0000 0000;
  uint32_t hasAssistMask = 0x4000000;  // 0000 0100 0000 0000 0000 0000 0000 0000;
  uint32_t assistIDMask = 0x78000000;  // 0111 1000 0000 0000 0000 0000 0000 0000;
  //type death
  uint32_t deathsMask = 0xFF80;        // 0000 0000 0000 0000 1111 1111 1000 0000;

  // how much each component needs to be shifted to the right
  uint32_t typeOffs = 0;
  uint32_t victimOffs = 3;
  uint32_t killOffs = 7;
  uint32_t offenderIDOffs = 15;
  uint32_t assistOffs = 19;
  uint32_t hasAssistOffs = 26;
  uint32_t assistIDOffs = 27;

  uint32_t deathOffs = 7;



  int type = (typeMask & receivedSignal) >> typeOffs;
  int victim;
  int deaths;
  int offender;
  int kills;
  int assistID;
  int assists;
  char radio_msg[200];
  switch (type) {
    case SIG_SETUP_FINISHED:
      Serial.println("Setup finished received");
      TelnetMsg("Setup finished received");
      break;
    case SIG_TIMER_STARTED:
      Serial.println("Timerstarted received");
      TelnetMsg("Timerstarted received");
      break;
    case SIG_GAME_OVER:
      Serial.println("Gameover received");
      TelnetMsg("Game Over received");
      break;
    case SIG_DEATH:
      victim = (victimIDMask & receivedSignal) >> victimOffs;
      deaths = (deathsMask & receivedSignal) >> deathOffs;
      sprintf(radio_msg,"Death message received: Player %u died %u times. \n", victim, deaths);
      Serial.println(radio_msg);
      TelnetMsg(radio_msg);
      break;
    case SIG_HIT:
      offender = (offenderIDMask & receivedSignal) >> offenderIDOffs;
      kills = (killsMask & receivedSignal) >> killOffs;
      victim = (victimIDMask & receivedSignal) >> victimOffs;
      sprintf(radio_msg,"Hit message received: Player %u killed player %u %u times. \n", offender, victim, kills);
      if ((hasAssistMask & receivedSignal) >> hasAssistOffs) {
        assistID = (assistIDMask & receivedSignal) >> assistIDOffs;
        assists = (assistsMask & receivedSignal) >> assistOffs;
        sprintf(radio_msg,"Hit message received: Player %u killed player %u %u times. \n Player %u has %u assists on Player \n%u", offender, victim, kills, assistID, assists, victim);

      }
      Serial.println(radio_msg);
      TelnetMsg(radio_msg);
      break;
    default:
      Serial.println("Unkown received");
      TelnetMsg("Unknown Signal received");
      break;
  }
}

void loopSpecific(){
  //!!!!!!!!!!!!!!!!!!1 DEACTIVATED
  if(0&&digitalRead(TRANSMIT_PIN) == HIGH){
    Serial.println("Detected hit");
    Wire.beginTransmission(DETECTOR_TWI_ADDRESS);
    Wire.write(I2C_GET_ALL); //Write start bit
    byte result =Wire.endTransmission();
    if(result != 0){
      char msg[40];
      sprintf(msg,"Failed to write address. Results: %u\n",result);
      Serial.print(msg);
      TelnetMsg(msg);
    }
    else{
      Wire.requestFrom(DETECTOR_TWI_ADDRESS,6); //Read results
      uint8_t results[6];
      for(int i=0;i<6;i++){
        if(Wire.available()){
          results[i]=Wire.read();//Reading from buffer into buffer here. Kind of stupid.
        }
      }
      if(results[0]==I2C_ALL_OK && results[5]==I2C_ALL_OK){
        //Everything ok
        uint8_t id = results[1] & 0b11111;
        uint8_t data = results[2] & 0b11111;

        char receiver_text[] = "Receiver: 0000 0000 - 0000 0000\n";
        sprintf(receiver_text, "Receiver: " BYTE_TO_BINARY_PATTERN " - " BYTE_TO_BINARY_PATTERN "\n", BYTE_TO_BINARY(results[4]) , BYTE_TO_BINARY(results[3]));
        char hit_msg[30];
        sprintf(hit_msg,"Hit by %u with data %u\n",id,data);
        Serial.print(hit_msg);
        Serial.print(receiver_text);
        TelnetMsg(hit_msg);
        TelnetMsg(receiver_text);

      }
      else if(results[0]==I2C_ERROR_BYTE && results[5]==I2C_ERROR_BYTE){
        Serial.println("The receiver is in an invalid state or there have been issues.");
        TelnetMsg("The receiver is in an invalid state or there have been issues.\n");
      }
      else{
        Serial.println("There have been issues with the I2C transmission. Buffer:");
        for(int i=0;i<6;i++){
          Serial.println(results[i],BIN);
        }
        TelnetMsg("There have been issues with the I2C transmission.\n");
      }
    }

    /*
    //Library brzo_i2c is more
    brzo_i2c_start_transaction(DETECTOR_ADDRESS,SCL_FREQ);
    uint8_t buffer[6];
    buffer[0] = 0x01;
    brzo_i2c_write(buffer,1,false); //Write initial address
    //brzo_i2c_read(buffer,6,false); //Read 6 consecutive bytes
    uint8_t result = brzo_i2c_end_transaction();
    Serial.printf("Result: %d\n",result);
    for(int i=0;i<6;i++){
      Serial.printf("Reading %d: ",i);
      Serial.println(buffer[i],BIN);
    }
    */
  }

  //listen to some radio
  if (rcs.available()) // if signal received
  {

    TelnetMsg("Avail\n");
    uint32_t value = rcs.getReceivedValue();

    decodeRadioSignal(value);

    rcs.resetAvailable();
  }
}

void onTelnetMessage(char* buffer){
  Serial.println(buffer);

  if(strncmp(buffer,"pause",strlen("pause"))==0){
    telnetSendQueue += "Pausing\n";

  }
  else if(strncmp(buffer,"resume",strlen("resume"))==0){
    telnetSendQueue += "Resuming\n";
  }
  else if(strncmp(buffer,"blink",strlen("blink"))==0){
    telnetSendQueue += "Blink\n";
    blinkLED();
  }
}
