# Debugging Tools

Code for ESP8266s used as debugging clients.

They can perform specific actions (e.g. shoot a specific ID or listen for hits).

They are controlled via telnet (on port 23) and log to telnet (and serial).

You need to create a file wifi.h in the projects src directory to provide the ssid and password for a wifi network to connect to. If it is not found a hotspot with password "minecraft95" is started.

```
#define WIFI "MySuperCoolWifi"
#define WIFI_PASS "Topscret"

```

