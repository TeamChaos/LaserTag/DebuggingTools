

/*
 * Copyright (c) 2015, Majenko Technologies
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * * Neither the name of Majenko Technologies nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Create a WiFi access point and provide a web server on it. */


//Basic
#include <WiFiClient.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include "wifi.h"


//IR Shoot

#include <IRtimer.h>
#include <IRrecv.h>
#include <IRremoteESP8266.h>
#include <IRsend.h>
#include "ir/protocol.h"

//radio
#include <RCSwitch.h>

#define SIG_SETUP_FINISHED 0
#define SIG_TIMER_STARTED 1
#define SIG_HIT 2
#define SIG_DEATH 3
#define SIG_GAME_OVER 4




//Wifi Configuration
const char* ssid = WIFI;
const char* password = WIFI_PASS;

const char* ssid_ap = "LaserTag_Debugger2";
const char* password_ap = "minecraft95";

//Telnet Configuration
#define INPUT_SIZE 50
#define MAX_TELNET_CLIENTS 1
WiFiServer TelnetServer(23);
WiFiClient TelnetClient[MAX_TELNET_CLIENTS];
uint8_t i;
bool ConnectionEstablished; // Flag for successfully handled connection

String telnetSendQueue="";

//radio
RCSwitch rcs = RCSwitch();






//Predefine basic methods
void TelnetMsg(String text);
void blinkLED();
void Telnet();


void setupSpecific();
void loopSpecific();
void onTelnetMessage(char* buffer);




//------------------------ BASIC MAIN -------------------------------------



void setup()
{
  Serial.begin(115200);
  Serial.println("Over The Air and Telnet Example");

  Serial.printf("Sketch size: %u\n", ESP.getSketchSize());
  Serial.printf("Free size: %u\n", ESP.getFreeSketchSpace());

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);



  // ... Give ESP 10 seconds to connect to station.
  unsigned long startTime = millis();
  Serial.print("Waiting for wireless connection ");
  while (WiFi.status() != WL_CONNECTED && millis() - startTime < 10000)
  {
    delay(200);
    Serial.print(".");
  }
  Serial.println();

  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.println("Connection Failed! Rebooting...");
    Serial.print("Configuring access point...");
    /* You can remove the password parameter if you want the AP to be open. */
    WiFi.softAP(ssid_ap, password_ap);

   IPAddress myIP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(myIP);
    //delay(3000);
    //ESP.restart();
  }

  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Starting Telnet server");
  TelnetServer.begin();
  TelnetServer.setNoDelay(true);

  pinMode(BUILTIN_LED, OUTPUT);  // initialize onboard LED as output

  // OTA

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  // ArduinoOTA.setHostname("myesp8266");

  // No authentication by default
  //ArduinoOTA.setPassword((const char *)"test");

  ArduinoOTA.onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH) {
        type = "sketch";
      } else { // U_SPIFFS
        type = "filesystem";
      }

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    });
    ArduinoOTA.onEnd([]() {
      Serial.println("\nEnd");
    });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    });
    ArduinoOTA.onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) {
        Serial.println("Auth Failed");
      } else if (error == OTA_BEGIN_ERROR) {
        Serial.println("Begin Failed");
      } else if (error == OTA_CONNECT_ERROR) {
        Serial.println("Connect Failed");
      } else if (error == OTA_RECEIVE_ERROR) {
        Serial.println("Receive Failed");
      } else if (error == OTA_END_ERROR) {
        Serial.println("End Failed");
      }
    });
  ArduinoOTA.begin();

  setupSpecific();
}

void loop() {
  ArduinoOTA.handle(); // Wait for OTA connection
  Telnet();  // Handle telnet connections
  if(telnetSendQueue.length() >0){
    TelnetMsg(telnetSendQueue);
    telnetSendQueue="";
  }
  loopSpecific();
}

void TelnetMsg(String text)
{
  for(i = 0; i < MAX_TELNET_CLIENTS; i++)
  {
    if (TelnetClient[i] && TelnetClient[i].connected())
    {
      TelnetClient[i].print(text);
    }
  }
  delay(10);  // to avoid strange characters left in buffer
}

void Telnet()
{
  // Cleanup disconnected session
  for(i = 0; i < MAX_TELNET_CLIENTS; i++)
  {
    if (TelnetClient[i] && !TelnetClient[i].connected())
    {
      Serial.print("Client disconnected ... terminate session "); Serial.println(i+1);
      TelnetClient[i].stop();
    }
  }

  // Check new client connections
  if (TelnetServer.hasClient())
  {
    ConnectionEstablished = false; // Set to false

    for(i = 0; i < MAX_TELNET_CLIENTS; i++)
    {
      // Serial.print("Checking telnet session "); Serial.println(i+1);

      // find free socket
      if (!TelnetClient[i])
      {
        TelnetClient[i] = TelnetServer.available();

        Serial.print("New Telnet client connected to session "); Serial.println(i+1);

        TelnetClient[i].flush();  // clear input buffer, else you get strange characters
        TelnetClient[i].println("Welcome!");

        TelnetClient[i].print("Millis since start: ");
        TelnetClient[i].println(millis());

        TelnetClient[i].print("Free Heap RAM: ");
        TelnetClient[i].println(ESP.getFreeHeap());

        TelnetClient[i].println("----------------------------------------------------------------");

        ConnectionEstablished = true;

        break;
      }
      else
      {
        // Serial.println("Session is in use");
      }
    }

    if (ConnectionEstablished == false)
    {
      Serial.println("No free sessions ... drop connection");
      TelnetServer.available().stop();
      // TelnetMsg("An other user cannot connect ... MAX_TELNET_CLIENTS limit is reached!");
    }
  }

  for(i = 0; i < MAX_TELNET_CLIENTS; i++)
  {
    if (TelnetClient[i] && TelnetClient[i].connected())
    {
      if(TelnetClient[i].available())
      {
        //get data from the telnet client
        while(TelnetClient[i].available())
        {
          char buffer[INPUT_SIZE + 1];
          byte size = TelnetClient[i].readBytesUntil('\n', buffer, INPUT_SIZE); //Probably should not use String because overhead
          buffer[size]=0;//Add 0 for c string end
          onTelnetMessage(buffer);
        }
      }
    }
  }
}

int ledState = LOW;

void blinkLED()
{
    // toggle the LED
    ledState = !ledState;
    digitalWrite(BUILTIN_LED, ledState);
    delay(20);

}


//------------- END MAIN --------------------------------




//------------- START SPECIFIC ------------------------


//Predeclare loopSpecific
void shoot(uint8_t id,uint8_t data);

void transmitHit(uint victim, uint offender, uint kills, bool hasAssist, uint assistID, uint assists);
void transmitDeath(uint victim, uint deaths);

//IR
IRsend irsend(4);
uint8_t last_id = 0xFF;
uint8_t last_data;

//radio
uint lastRadioSignal = 0;



void setupSpecific(){
  irsend.begin();
    rcs.enableTransmit(12);
}

void loopSpecific(){

}

void onTelnetMessage(char* buffer){
  Serial.println(buffer);

  if(strncmp(buffer,"s",strlen("s"))==0){
    uint arg1;
    uint arg2;
    int args = sscanf(buffer,"s %u %u",&arg1,&arg2);
    Serial.printf("Last values %u %u",last_id,last_data);
    Serial.printf("Shoot with %d args (%u %u)\n",args,arg1,arg2);
    if(args == 0 || args == -1){
      if(last_id==0xFF){
        telnetSendQueue = telnetSendQueue + "You can only use shoot without arguments if you have shot before\n";
      }
      else{
        shoot(last_id,last_data);
      }
    }
    else if(args ==1){
      if(last_id==0xFF){
        telnetSendQueue = telnetSendQueue + "You can only use shoot with just one argument if you have shot before\n";
      }
      else{
        last_data = arg1;
        shoot(last_id,last_data);
      }
    }
    else if(args==2){
      last_id=arg1;
      last_data=arg2;
      shoot(last_id,last_data);
    }
    else{
      telnetSendQueue = telnetSendQueue + "Unknown shoot command\n";
    }
  }
  else if(strncmp(buffer,"r hit",strlen("r hit"))==0){
    uint arg1;
    uint arg2;
    uint arg3;
    uint arg4;
    uint arg5;

    int args = sscanf(buffer,"r hit %u %u %u %u %u",&arg1,&arg2,&arg3,&arg4,&arg5);
    if (args == 0) {
      telnetSendQueue = telnetSendQueue + "Use something like: >>r hit <victimID> <offenderID> <kills> [<assistID> <assists>]\n";
    }
    else if(args == 3){
      transmitHit(arg1, arg2, arg3, false, 0, 0);
    }
    else if(args == 5){
      transmitHit(arg1, arg2, arg3, true, arg4, arg5);
    }
  }
  else if(strncmp(buffer,"r death",strlen("r death"))==0){
    uint arg1;
    uint arg2;

    int args = sscanf(buffer,"r death %u %u",&arg1,&arg2);
    if (args == 0) {
      telnetSendQueue = telnetSendQueue + "Use something like: >>r death <victimID> <deaths>\n";
    }
    else if(args == 2){
      transmitDeath(arg1, arg2);
    }
  }
  else if(strncmp(buffer,"r",strlen("r"))==0){
    if (lastRadioSignal != 0) {
      rcs.send(lastRadioSignal, 32);
    }
  }
  else if(strncmp(buffer,"blink",strlen("blink"))==0){
    telnetSendQueue += "Blink\n";
    blinkLED();
  }
}


void shoot(uint8_t id, uint8_t data){

  uint16_t raw = ((id & 0b11111)<<6) | ((data & 0b11111) << 1);
  raw |= __builtin_parity(raw) & 0b1;
  irsend.sendGeneric(START_MARK,START_SPACE,BIT_MARK,ONE_SPACE,BIT_MARK,ZERO_SPACE,BIT_MARK,1000U,raw,11,38,true,0,50);
  telnetSendQueue = telnetSendQueue + "Shot!\n";
}

void transmitHit(uint victim, uint offender, uint kills, bool hasAssist, uint assistID, uint assists) {
  uint32_t radioSignal = SIG_HIT;

  uint32_t victimOffs = 3;
  uint32_t killOffs = 7;
  uint32_t offenderIDOffs = 15;
  uint32_t assistOffs = 19;
  uint32_t hasAssistOffs = 26;
  uint32_t assistIDOffs = 27;

  radioSignal = radioSignal | (victim << victimOffs);
  radioSignal = radioSignal | (kills << killOffs);
  radioSignal = radioSignal | (offender << offenderIDOffs);

  if (hasAssist) {
    radioSignal = radioSignal | (assists << assistOffs);
    radioSignal = radioSignal | (1 << hasAssistOffs);
    radioSignal = radioSignal | (assistID << assistIDOffs);
  }

  rcs.send(radioSignal, 32);

  telnetSendQueue = telnetSendQueue + "Radio sais, someone died!\n";

  lastRadioSignal = radioSignal;
}

void transmitDeath(uint victim, uint deaths) {
  uint radioSignal = SIG_DEATH;

  uint32_t victimOffs = 3;
  uint32_t deathOffs = 7;

  radioSignal = radioSignal | (victim << victimOffs);
  radioSignal = radioSignal | (deaths << deathOffs);

  rcs.send(radioSignal, 32);

  telnetSendQueue = telnetSendQueue + "Radio sais, someone died!\n";

  lastRadioSignal = radioSignal;

}
